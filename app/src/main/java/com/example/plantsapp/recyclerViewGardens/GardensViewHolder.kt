package com.example.plantsapp.recyclerViewGardens

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.R

class GardensViewHolder (itemView: View): RecyclerView.ViewHolder(itemView){
    val gardenName = itemView.findViewById<TextView>(R.id.gardenName_textView)
    val gardenImage = itemView.findViewById<ImageView>(R.id.garden_imageView)
    val gardenID = itemView.findViewById<TextView>(R.id.garden_id_textView)
}