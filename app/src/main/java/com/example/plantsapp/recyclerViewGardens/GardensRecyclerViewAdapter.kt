package com.example.plantsapp.recyclerViewGardens

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.GardensFragmentDirections
import com.example.plantsapp.R
import com.example.plantsapp.classes.GardenRecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squareup.picasso.Picasso

class GardensRecyclerViewAdapter: RecyclerView.Adapter<GardensViewHolder>(){

    val db = FirebaseFirestore.getInstance()
        val gardens: MutableList<GardenRecyclerView> = mutableListOf<GardenRecyclerView>()
    val firebaseUser = FirebaseAuth.getInstance().currentUser!!

    init {
        val userRef = db.collection("users").document(firebaseUser.uid)
        val plantsRef = userRef.collection("gardens")

        plantsRef
            .orderBy("gardenName", Query.Direction.ASCENDING)
            .addSnapshotListener{snapshot, error ->
                if (error != null){
                    //Show alert
                    return@addSnapshotListener
                }

                for (doc in snapshot!!.documentChanges){
                    when(doc.type){
                        DocumentChange.Type.ADDED -> {
                            Log.d("gardennn", doc.document.getString("gardenName")!!)
                            val garden = GardenRecyclerView(
                                doc.document.id,
                                doc.document.getString("gardenName")!!,
                                doc.document.getString("imageURL")!!
                            )
                            gardens.add(garden)
                            notifyItemInserted(gardens.size - 1)
                        }
                        else -> return@addSnapshotListener
                    }
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GardensViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.gardens_view_holder, parent, false)
        return GardensViewHolder(view)
    }

    override fun getItemCount(): Int { //Cuantos items hay en el View Holder
        return gardens.size
    }

    override fun onBindViewHolder(holder: GardensViewHolder, position: Int) {
        holder.gardenID.text = gardens[position].gardenID
        holder.gardenName.text = gardens[position].gardenName
        if (gardens[position].imageURL != "") {
            Picasso.with(holder.gardenImage.context).load(gardens[position].imageURL).into(holder.gardenImage)
        }
        holder.itemView.setOnClickListener{
            Log.d("recyclerview", gardens[position].gardenID)
            val action = GardensFragmentDirections.actionGardensFragmentToSeedTimeFragment(gardens[position].gardenID)
            holder.itemView.findNavController().navigate(action)
        }
    }
}