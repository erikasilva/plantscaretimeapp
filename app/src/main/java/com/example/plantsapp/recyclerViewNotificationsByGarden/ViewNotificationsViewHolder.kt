package com.example.plantsapp.recyclerViewNotifications

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.R

class ViewNotificationsViewHolder (itemView: View): RecyclerView.ViewHolder(itemView){
    val alarmName = itemView.findViewById<TextView>(R.id.alarmName_textView_id)
    val alarmDescription = itemView.findViewById<TextView>(R.id.alarmDescription_textView_id)
    val alarmDate = itemView.findViewById<TextView>(R.id.alarmDate_textView_id)
    val alarmTime = itemView.findViewById<TextView>(R.id.alarmTime_textView_id)
    val alarmImage = itemView.findViewById<ImageView>(R.id.alarm_imageView_id)
    val alarmID = itemView.findViewById<TextView>(R.id.alarm_id_textView)
}