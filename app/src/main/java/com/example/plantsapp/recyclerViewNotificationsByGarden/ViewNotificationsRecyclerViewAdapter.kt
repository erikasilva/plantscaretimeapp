package com.example.plantsapp.recyclerViewNotifications

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.R
import com.example.plantsapp.classes.AlarmRecyclerView
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squareup.picasso.Picasso
import java.util.*



class ViewNotificationsRecyclerViewAdapter(gardenID: String): RecyclerView.Adapter<ViewNotificationsViewHolder>(){

    val db = FirebaseFirestore.getInstance()
    val alarms: MutableList<AlarmRecyclerView> = mutableListOf()
    val firebaseUser = FirebaseAuth.getInstance().currentUser!!

    init {
        val userRef = db.collection("users").document(firebaseUser.uid)
        val alarmsRef = userRef.collection("alarms")

        alarmsRef.whereEqualTo("gardenID", gardenID)
            .orderBy("alarmDate", Query.Direction.DESCENDING)
            .addSnapshotListener{snapshot, error ->
                if (error != null){
                    //Show alert
                    return@addSnapshotListener
                }

                for (doc in snapshot!!.documentChanges){
                    when(doc.type){
                        DocumentChange.Type.ADDED -> {
                            val alarm = AlarmRecyclerView(
                                doc.document.id,
                                doc.document.getString("alarmName")!!,
                                doc.document.getString("alarmDescription")!!,
                                doc.document.getTimestamp("alarmDate")!!,
                                doc.document.getString("imageURL")!!
                            )
                            alarms.add(alarm)
                            notifyItemInserted(alarms.size - 1)
                        }
                        else -> return@addSnapshotListener
                    }
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewNotificationsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.notifications_view_holder, parent, false)
        return ViewNotificationsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return alarms.size
    }

    private fun getDate(date: Date): String{
        val day = date.date.toString()
        val month = (date.month + 1).toString()
        val year = (date.year + 1900).toString()
        return "$day/$month/$year"
    }

    private fun getTime(date: Date): String{
        val hours = date.hours.toString()
        val minutes = date.minutes.toString()
        return "$hours:$minutes"
    }

    override fun onBindViewHolder(holder: ViewNotificationsViewHolder, position: Int) {
        holder.alarmID.text = alarms[position].alarmID
        holder.alarmName.text = alarms[position].alarmName
        holder.alarmDescription.text = alarms[position].alarmDescription
        holder.alarmDate.text = getDate(alarms[position].alarmDate.toDate())
        holder.alarmTime.text = getTime(alarms[position].alarmDate.toDate())
        if (alarms[position].imageURL != ""){
            Picasso.with(holder.alarmImage.context).load(alarms[position].imageURL).into(holder.alarmImage)
        }
        holder.itemView.setOnClickListener{

        }
    }
}