package com.example.plantsapp.recyclerViewSeedTime

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.R

class SeedTimeViewHolder (itemView: View): RecyclerView.ViewHolder(itemView){
    val seedTimeName = itemView.findViewById<TextView>(R.id.seedTimeName_textView)
    val seedTimeSownPlants = itemView.findViewById<TextView>(R.id.seedTimeSownPlants_textView)
    val seedTimeImage = itemView.findViewById<ImageView>(R.id.seedTime_imageView)
    val seedTimeID = itemView.findViewById<TextView>(R.id.seedTime_id_textView)
}