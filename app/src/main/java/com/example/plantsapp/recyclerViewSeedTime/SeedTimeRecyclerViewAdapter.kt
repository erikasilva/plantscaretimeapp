package com.example.plantsapp.recyclerViewSeedTime

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.R
import com.example.plantsapp.SeedTimeFragmentDirections
import com.example.plantsapp.classes.Plant
import com.example.plantsapp.classes.SeedTimeRecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squareup.picasso.Picasso

class SeedTimeRecyclerViewAdapter(gardenID: String): RecyclerView.Adapter<SeedTimeViewHolder>(){

    var gardenID:String? = ""
    val db = FirebaseFirestore.getInstance()
    val seedtime: MutableList<SeedTimeRecyclerView> = mutableListOf<SeedTimeRecyclerView>()
    var gardenid = gardenID
    val firebaseUser = FirebaseAuth.getInstance().currentUser!!

    init {
        val userRef = db.collection("users").document(firebaseUser.uid)
        val plantsRef = userRef.collection("gardens").document(gardenID).collection("seedtime")

        plantsRef
            .orderBy("sownPlants", Query.Direction.DESCENDING)
            .addSnapshotListener{snapshot, error ->
                if (error != null){
                    //Show alert
                    return@addSnapshotListener
                }

                for (doc in snapshot!!.documentChanges){
                    when(doc.type){
                        DocumentChange.Type.ADDED -> {
                            getPlantInfo(doc.document.getString("plantID")!!, doc)
                        }
                        else -> return@addSnapshotListener
                    }
                }
            }
    }

    private fun loadSeedTimeInformationInObject(doc: DocumentChange, plant: Plant){
        val sowing = SeedTimeRecyclerView(
            doc.document.id,
            doc.document.getString("plantID")!!,
            doc.document.getDouble("sownPlants")!!.toInt(),
            plant.plantName,
            plant.imageURL
        )
        seedtime.add(sowing)
        notifyItemInserted(seedtime.size - 1)
    }

    private fun getPlantInfo(plantID: String, doc: DocumentChange){
        val plantRef = db.collection("users").document(firebaseUser.uid).collection("plants").document(plantID)
        var plant: Plant?
        plantRef.get().addOnCompleteListener{ documentSnapshot ->
            plant = documentSnapshot.result?.toObject(Plant::class.java)
            loadSeedTimeInformationInObject(doc, plant!!)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeedTimeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.seedtime_view_holder, parent, false)
        return SeedTimeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return seedtime.size
    }

    override fun onBindViewHolder(holder: SeedTimeViewHolder, position: Int) {
        holder.seedTimeID.text = seedtime[position].seedTimeID
        holder.seedTimeName.text = "Siembra de ${seedtime[position].plantName}"
        holder.seedTimeSownPlants.text = "Plantas sembradas: ${seedtime[position].sownPlants}"
        if (seedtime[position].imageURL != ""){
            Picasso.with(holder.seedTimeImage.context).load(seedtime[position].imageURL).into(holder.seedTimeImage)
        }
        holder.itemView.setOnClickListener{
            val action = SeedTimeFragmentDirections.actionSeedTimeFragmentToEditSeedTimeFragment(seedtime[position].seedTimeID, gardenid)
            holder.itemView.findNavController().navigate(action)
        }
    }
}