package com.example.plantsapp.notifications

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.appcompat.widget.DialogTitle
import kotlin.math.log

class AlarmReceiver(): BroadcastReceiver(){

    override fun onReceive(context: Context?, intent: Intent?) {
        val service = Intent(context, NotificationService::class.java)
        service.putExtra("reason", intent?.getStringExtra("reason"))
        service.putExtra("timestamp", intent?.getLongExtra("timestamp", 0))
        service.putExtra("title", intent?.getStringExtra("title"))
        service.putExtra("description", intent?.getStringExtra("description"))
        context?.startService(service)
    }
}