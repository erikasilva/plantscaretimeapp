package com.example.plantsapp.notifications

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.ClipDescription
import android.content.Intent
import android.os.SystemClock
import android.util.Log
import java.util.*

class NotificationsUtils{
    fun setNotification(timeInMilliSeconds: Long, activity: Activity, title: String, description: String) {
        if (timeInMilliSeconds > 0) {
            val alarmManager = activity.getSystemService(Activity.ALARM_SERVICE) as AlarmManager
            val alarmIntent = Intent(activity.applicationContext, AlarmReceiver::class.java)
            alarmIntent.putExtra("reason", "notification")
            alarmIntent.putExtra("timestamp", timeInMilliSeconds)
            alarmIntent.putExtra("title", title)
            alarmIntent.putExtra("description", description)
            val calendar = Calendar.getInstance()
            val futureMills = SystemClock.elapsedRealtime() + timeInMilliSeconds
            Log.d("Difference", futureMills.toString())
            calendar.timeInMillis = futureMills
            val pendingIntent = PendingIntent.getBroadcast(activity, 0, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT)
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureMills, pendingIntent)
        }
    }
}