package com.example.plantsapp

import android.os.Bundle
import android.util.Log
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_navigation.*


class NavigationActivity : AppCompatActivity() {

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val nav = mainFragment.findNavController()
        when (item.itemId) {
            R.id.navigation_notifications -> {
                nav.navigate(R.id.notificationsFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_plants -> {
                nav.navigate(R.id.plantsFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_gardens -> {
                nav.navigate(R.id.gardensFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                nav.navigate(R.id.accountFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }
}
