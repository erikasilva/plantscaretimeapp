package com.example.plantsapp


import android.graphics.Canvas
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.notifications.NotificationsUtils
import com.example.plantsapp.recyclerViewNotifications.NotificationsRecyclerViewAdapter
import com.example.plantsapp.recyclerViewNotifications.SwipeController
import com.example.plantsapp.recyclerViewNotifications.SwipeControllerActions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore

class NotificationsFragment : Fragment() {

    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseUser: FirebaseUser
    private lateinit var addAlarmButton: FloatingActionButton
    private lateinit var notificationsRecyclerView: RecyclerView
    lateinit var swipeController: SwipeController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        db = FirebaseFirestore.getInstance()
        firebaseUser = FirebaseAuth.getInstance().currentUser!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addAlarmButton = view.findViewById(R.id.addAlarmButton_id)
        addAlarmButton.setOnClickListener {goToCreateNewAlarm()}
        val notificationsRecyclerViewAdapter = NotificationsRecyclerViewAdapter()
        notificationsRecyclerView  = view.findViewById(R.id.notificationsRecyclerView_id)
        notificationsRecyclerView.layoutManager = LinearLayoutManager(this.requireContext())
        notificationsRecyclerView.adapter = notificationsRecyclerViewAdapter

        swipeController = SwipeController(object : SwipeControllerActions() {
            override fun onRightClicked(position: Int) {
                updateAlarmStatus(notificationsRecyclerViewAdapter.alarms[position].alarmID)
                notificationsRecyclerViewAdapter.alarms.removeAt(position)
                notificationsRecyclerViewAdapter.notifyItemRemoved(position)
                notificationsRecyclerViewAdapter.notifyItemRangeChanged(position, notificationsRecyclerViewAdapter.itemCount)
            }
        })

        val itemTouchHelper = ItemTouchHelper(swipeController)
        itemTouchHelper.attachToRecyclerView(notificationsRecyclerView)
        notificationsRecyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
                swipeController.onDraw(c)
            }
        })
    }

    private fun updateAlarmStatus(id: String){
        db.collection("users")
            .document(firebaseUser.uid).collection("alarms").document(id)
            .update("status", "Canceled")
            .addOnSuccessListener { documentReference ->
                Log.d("Status changed", "DocumentSnapshot added with ID: $documentReference")
            }
            .addOnFailureListener { e ->
                Log.w("Status changed", "Error adding document", e)
            }
    }

    private fun goToCreateNewAlarm(){
        val action = NotificationsFragmentDirections.actionNotificationsFragmentToNewAlarmFragment()
        view?.findNavController()?.navigate(action)
    }
}
