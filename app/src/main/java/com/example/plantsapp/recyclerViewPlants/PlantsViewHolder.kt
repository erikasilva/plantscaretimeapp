package com.example.plantsapp.recyclerViewPlants

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.R

class PlantsViewHolder (itemView: View): RecyclerView.ViewHolder(itemView){
    val plantName = itemView.findViewById<TextView>(R.id.plantName_textView)
    val plantImage = itemView.findViewById<ImageView>(R.id.plant_imageView)
    val plantID = itemView.findViewById<TextView>(R.id.plant_id_textView)
}