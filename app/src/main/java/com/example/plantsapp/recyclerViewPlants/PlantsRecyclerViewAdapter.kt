package com.example.plantsapp.recyclerViewPlants

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.PlantsFragmentDirections
import com.example.plantsapp.R
import com.example.plantsapp.classes.PlantRecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_login.view.*

class PlantsRecyclerViewAdapter: RecyclerView.Adapter<PlantsViewHolder>(){

    val db = FirebaseFirestore.getInstance()
    val plants: MutableList<PlantRecyclerView> = mutableListOf<PlantRecyclerView>()
    val firebaseUser = FirebaseAuth.getInstance().currentUser!!

    init {
        val userRef = db.collection("users").document(firebaseUser.uid)
        val plantsRef = userRef.collection("plants")

        plantsRef
            .orderBy("plantName", Query.Direction.ASCENDING)
            .addSnapshotListener{snapshot, error ->
                if (error != null){
                    //Show alert
                    return@addSnapshotListener
                }

                for (doc in snapshot!!.documentChanges){
                    when(doc.type){
                        DocumentChange.Type.ADDED -> {
                            Log.d("plpants", doc.document.getString("plantName"))
                            val plant = PlantRecyclerView(
                                doc.document.id,
                                doc.document.getString("plantName")!!,
                                doc.document.getString("imageURL")!!
                            )
                            plants.add(plant)
                            notifyItemInserted(plants.size - 1)
                        }
                        else -> return@addSnapshotListener
                    }
                }
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlantsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.plants_view_holder, parent, false)
        return PlantsViewHolder(view)
    }

    override fun getItemCount(): Int { //Cuantos items hay en el View Holder
        return plants.size
    }

    override fun onBindViewHolder(holder: PlantsViewHolder, position: Int) { //Que informacion le corresponde a cada view holder
        holder.plantID.text = plants[position].plantID
        holder.plantName.text = plants[position].plantName
        if (plants[position].imageURL != ""){
            Picasso.with(holder.plantImage.context).load(plants[position].imageURL).into(holder.plantImage)
        }

        holder.itemView.setOnClickListener{
            Log.d("recyclerview", plants[position].plantID)
            val action = PlantsFragmentDirections.actionPlantsFragmentToEditPlantFragment(plants[position].plantID)
            holder.itemView?.findNavController()?.navigate(action)
        }
    }
}