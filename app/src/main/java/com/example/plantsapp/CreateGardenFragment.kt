package com.example.plantsapp


import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.findNavController
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso

class CreateGardenFragment : Fragment() {

    private lateinit var storage: FirebaseStorage
    private lateinit var storageRef: StorageReference
    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseUser: FirebaseUser
    private lateinit var imageGardenButton: ImageButton
    private lateinit var gardenNameText: TextInputEditText
    private lateinit var gardenLocationText: TextInputEditText
    private lateinit var gardenTypesText: AutoCompleteTextView
    private lateinit var cancelButton: Button
    private lateinit var saveButton: Button
    private var imageURI: Uri? = null
    private val GALLERY = 1
    private val CAMERA = 2

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_garden, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val types = arrayOf("Huerta", "Jardin", "Parcela")
        loadItemsInDropdown(types, R.id.gardenTypes_dropdown_id)
        imageGardenButton = view.findViewById(R.id.gardenImage_id)
        gardenTypesText = view.findViewById(R.id.gardenTypes_dropdown_id)
        gardenNameText = view.findViewById(R.id.gardenName_id)
        gardenLocationText = view.findViewById(R.id.gardenLocation_id)
        cancelButton = view.findViewById(R.id.cancelButton_id)
        saveButton = view.findViewById(R.id.saveButton_id)
        imageGardenButton.setOnClickListener { uploadImage(this.requireContext(), this) }
        saveButton.setOnClickListener { saveNewGarden() }
        cancelButton.setOnClickListener { goToGardensFragment() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        db = FirebaseFirestore.getInstance()
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference
        firebaseUser = FirebaseAuth.getInstance().currentUser!!
        super.onCreate(savedInstanceState)
    }

    private fun loadItemsInDropdown(items: Array<String>, dropdown: Int) {
        val adapter = ArrayAdapter(
            this.requireContext(),
            R.layout.dropdown_menu_popup_item,
            items
        )
        val editTextFilledExposedDropdown = view?.findViewById<AutoCompleteTextView>(dropdown)
        editTextFilledExposedDropdown?.setAdapter(adapter)
    }

    override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY)
        {
            if (data != null)
            {
                imageURI = data.data
                Picasso.with(this.requireContext()).load(imageURI).into(imageGardenButton)
            }
        }
        else if (requestCode == CAMERA)
        {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            imageGardenButton.setImageBitmap(thumbnail)
        }
    }

    private fun saveNewGarden(){
        val gardenName = gardenNameText.text.toString()
        val gardenLocation = gardenLocationText.text.toString()
        val gardenTypes = gardenTypesText.text.toString()

        val newGarden = hashMapOf(
            "gardenName" to gardenName,
            "gardenType" to gardenTypes,
            "gardenLocation" to gardenLocation,
            "imageURL" to ""
        )

        db.collection("users")
            .document(firebaseUser.uid).collection("gardens")
            .add(newGarden)
            .addOnSuccessListener { documentReference ->
                Log.d("SaveGarden", "DocumentSnapshot added with ID: ${documentReference.id}")
                Toast.makeText(this.requireContext(), "Datos Guardados", Toast.LENGTH_LONG).show()
                if (imageURI != null){
                    saveImageInFirebase(documentReference.id)
                }
            }
            .addOnFailureListener { e ->
                Log.w("SaveGarden", "Error adding document", e)
                Toast.makeText(this.requireContext(), "Error, no se pudo guardar la información", Toast.LENGTH_LONG).show()
            }
        goToGardensFragment()
    }

    private fun saveImageInFirebase(id: String){
        val imagesRef: StorageReference? = storageRef.child("images/${firebaseUser.uid}/gardens/$id")
        val uploadTask = imagesRef?.putFile(imageURI!!)
        uploadTask?.addOnFailureListener {
            Log.d("SaveImage", "Error")
        }?.addOnSuccessListener {
            imagesRef.downloadUrl.addOnSuccessListener {
                    uri -> saveImageURLInFirestore(uri.toString(), id)
            }
            Log.d("SaveImage", "File upload")
        }
    }

    private fun saveImageURLInFirestore(imageURL: String, id: String){
        db.collection("users")
            .document(firebaseUser.uid).collection("gardens").document(id)
            .update("imageURL", imageURL)
            .addOnSuccessListener { documentReference ->
                Log.d("SaveImageURL", "DocumentSnapshot added with ID: $documentReference")
            }
            .addOnFailureListener { e ->
                Log.w("SaveImageURL", "Error adding document", e)
            }
    }

    private fun goToGardensFragment(){
        val action = CreateGardenFragmentDirections.actionCreateGardenFragmentToGardensFragment()
        view?.findNavController()?.navigate(action)
    }
}
