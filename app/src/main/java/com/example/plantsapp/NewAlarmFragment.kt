package com.example.plantsapp

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.plantsapp.classes.*
import com.example.plantsapp.notifications.NotificationsUtils
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.seedtime_view_holder.*
import java.time.LocalDateTime
import java.util.*



class NewAlarmFragment : Fragment() {

    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseUser: FirebaseUser
    private lateinit var alarmNameText: TextInputEditText
    private lateinit var gardenText: AutoCompleteTextView
    private lateinit var seedTimeText: AutoCompleteTextView
    private lateinit var alarmTypeText: AutoCompleteTextView
    private lateinit var alarmDateText: TextInputEditText
    private lateinit var alarmTimeText: TextInputEditText
    private lateinit var saveButton: Button
    private lateinit var cancelButton: Button
    private var garden:GardenRecyclerView? = null
    private var seedTime:SeedTimeRecyclerView? = null
    private var seedTimeArray: MutableList<SeedTimeRecyclerView> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_alarm, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        db = FirebaseFirestore.getInstance()
        firebaseUser = FirebaseAuth.getInstance().currentUser!!

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        alarmNameText = view.findViewById(R.id.alarmName_id)
        gardenText = view.findViewById(R.id.gardens_dropdown_id)
        seedTimeText = view.findViewById(R.id.seedTime_dropdown_id)
        alarmTypeText = view.findViewById(R.id.alarmType_dropdown_id)
        alarmDateText = view.findViewById(R.id.alarmDateText_id)
        alarmTimeText = view.findViewById(R.id.alarmTimeText_id)
        saveButton = view.findViewById(R.id.saveButton_id)
        cancelButton = view.findViewById(R.id.cancelButton_id)
        alarmDateText.isFocusable = false
        alarmDateText.setOnClickListener{openDatePicker(alarmDateText, this.requireActivity())}
        alarmTimeText.isFocusable = false
        alarmTimeText.setOnClickListener{openTimePicker(alarmTimeText, this.requireActivity())}
        cancelButton.setOnClickListener { goToNotificationsFragment() }
        loadItemsInGardensDropdown(loadGardens(), R.id.gardens_dropdown_id)
        gardenText.setOnItemClickListener { parent, _, position, _ ->
            seedTimeArray.removeAll(seedTimeArray)
            seedTimeText.setText("")
            garden = parent.getItemAtPosition(position) as GardenRecyclerView
            loadItemsInSeedTimeDropdown(loadSeedTime(garden!!.gardenID), R.id.seedTime_dropdown_id)
        }

        seedTimeText.setOnItemClickListener { parent, view, position, id ->
            seedTime = parent.getItemAtPosition(position) as SeedTimeRecyclerView
        }

        val types = arrayOf("Riego", "Fertilizante/Pesticida", "Podar", "Abono")
        loadItemsInAlarmTypeDropdown(types, R.id.alarmType_dropdown_id)
        saveButton.setOnClickListener { createNewAlarm() }
    }

    private fun loadItemsInAlarmTypeDropdown(items: Array<String>, dropdown: Int) {
        val adapter = ArrayAdapter(
            this.requireContext(),
            R.layout.dropdown_menu_popup_item,
            items
        )
        val editTextFilledExposedDropdown = view?.findViewById<AutoCompleteTextView>(dropdown)
        editTextFilledExposedDropdown?.setAdapter(adapter)
    }

    private fun loadItemsInGardensDropdown(items: MutableList<GardenRecyclerView>, dropdown: Int) {
        val adapter = ArrayAdapter(
            this.requireContext(),
            R.layout.dropdown_menu_popup_item,
            items
        )
        val editTextFilledExposedDropdown = view?.findViewById<AutoCompleteTextView>(dropdown)
        editTextFilledExposedDropdown?.setAdapter(adapter)
    }

    private fun loadItemsInSeedTimeDropdown(items: MutableList<SeedTimeRecyclerView>, dropdown: Int) {
        val adapter = ArrayAdapter(
            this.requireContext(),
            R.layout.dropdown_menu_popup_item,
            items
        )
        val editTextFilledExposedDropdown = view?.findViewById<AutoCompleteTextView>(dropdown)
        editTextFilledExposedDropdown?.setAdapter(adapter)
    }

    private fun loadGardens(): MutableList<GardenRecyclerView> {
        val gardensArray: MutableList<GardenRecyclerView> = mutableListOf()
        var garden: Garden
        db.collection("users")
            .document(firebaseUser.uid).collection("gardens")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    garden = document.toObject(Garden::class.java)
                    gardensArray.add(GardenRecyclerView(document.id, garden.gardenName, ""))
                }
                if (gardensArray.size == 0){
                    Toast.makeText(this.requireContext(), "No tiene jardines creados", Toast.LENGTH_LONG).show()
                    goToNotificationsFragment()
                }
            }
            .addOnFailureListener { exception ->
                Log.d("GetPlant", "Error getting documents: ", exception)
            }
        return gardensArray
    }

    private fun loadSeedTime(gardenID: String): MutableList<SeedTimeRecyclerView> {
        seedTimeArray.add(SeedTimeRecyclerView("-1", "",0, "Todas las siembras", ""))
        var seedtime: SeedTime
        db.collection("users")
            .document(firebaseUser.uid).collection("gardens").document(gardenID).collection("seedtime")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    seedtime = document.toObject(SeedTime::class.java)
                    getPlantName(seedtime, document)
                }
            }
            .addOnFailureListener { exception ->
                Log.d("GetPlant", "Error getting documents: ", exception)
            }
        return seedTimeArray
    }

    private fun getPlantName(seedTime: SeedTime, document: DocumentSnapshot){
        val docRef =  db.collection("users")
            .document(firebaseUser.uid).collection("plants").document(seedTime.plantID)
        docRef.get()
            .addOnSuccessListener { documentSnapshot ->
                val plant = documentSnapshot.toObject(Plant::class.java)
                seedTimeArray.add(SeedTimeRecyclerView(document.id, seedTime.plantID, 0, plant!!.plantName, ""))
            }
    }

    private fun createNewAlarm(){
        val alarmTime = putTextInFormateDate(alarmDateText.text.toString().split("-"), alarmTimeText.text.toString().split(":"))
        val timeDifference = calculateDifferenceTimePeriods(alarmTime)
        Log.d("Difference", timeDifference.toString())
        val description = "Alarma de ${alarmTypeText.text} para ${seedTimeText.text} del jardin ${gardenText.text}"
        NotificationsUtils().setNotification(timeDifference, this.requireActivity(), alarmNameText.text.toString(), description)
        saveInformationInFirebase()
    }

    private fun getGardenImageURL(id: String){
        val docRef =  db.collection("users")
            .document(firebaseUser.uid).collection("gardens").document(garden!!.gardenID)
        docRef.get()
            .addOnSuccessListener { documentSnapshot ->
                saveImageInFirebase(documentSnapshot.getString("imageURL")!!, id)
            }
    }

    private fun saveImageInFirebase(imageURL: String, id: String){
        db.collection("users")
            .document(firebaseUser.uid).collection("alarms").document(id)
            .update("imageURL", imageURL)
            .addOnSuccessListener { documentReference ->
                Log.d("SaveImageURL", "DocumentSnapshot added with ID: $documentReference")
            }
            .addOnFailureListener { e ->
                Log.w("SaveImageURL", "Error adding document", e)
            }
    }

    private fun saveInformationInFirebase(){
        val alarmName = alarmNameText.text.toString()
        val gardenName = gardenText.text.toString()
        val seedTimeName = seedTimeText.text.toString()
        val alarmType = alarmTypeText.text.toString()
        val alarmDescription = "Alarma de $alarmType para $seedTimeName del jardin $gardenName"
        val alarmDateSplit = alarmDateText.text.toString().split("-")
        val alarmTimeSplit = alarmTimeText.text.toString().split(":")

        val newAlarm = hashMapOf(
            "alarmName" to alarmName,
            "alarmDescription" to alarmDescription,
            "alarmDate" to putTextInFormateDate(alarmDateSplit, alarmTimeSplit).time,
            "imageURL" to "",
            "status" to "Pending",
            "gardenID" to garden?.gardenID
        )

        db.collection("users")
            .document(firebaseUser.uid).collection("alarms")
            .add(newAlarm)
            .addOnSuccessListener { documentReference ->
                Log.d("SaveAlarm", "DocumentSnapshot added with ID: ${documentReference.id}}")
                Toast.makeText(this.requireContext(), "Datos Guardados", Toast.LENGTH_LONG).show()
                getGardenImageURL(documentReference.id)
                goToNotificationsFragment()
            }
            .addOnFailureListener { e ->
                Log.w("SaveAlarm", "Error adding document", e)
                Toast.makeText(this.requireContext(), "Error, no se pudo guardar la información", Toast.LENGTH_LONG).show()
            }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun calculateDifferenceTimePeriods(alarmTime: Calendar): Long{
        val currentTime = System.currentTimeMillis()
        return alarmTime.timeInMillis - currentTime
    }

    private fun putTextInFormateDate(dateSplit: List<String>, timeSplit: List<String>): Calendar{
        val year = dateSplit.component1().trim().toInt()
        val month = dateSplit.component2().trim().toInt()
        val day = dateSplit.component3().trim().toInt()
        val hour = timeSplit.component1().trim().toInt()
        val minute = timeSplit.component2().trim().toInt()

        val cal: Calendar = Calendar.getInstance()
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, month - 1)
        cal.set(Calendar.DATE, day)
        cal.set(Calendar.HOUR_OF_DAY, hour)
        cal.set(Calendar.MINUTE, minute)
        return cal
    }

    private fun goToNotificationsFragment(){
        val action = NewAlarmFragmentDirections.actionNewAlarmFragmentToNotificationsFragment()
        view?.findNavController()?.navigate(action)
    }
}
