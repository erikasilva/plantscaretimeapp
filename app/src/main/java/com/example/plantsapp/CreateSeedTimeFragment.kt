package com.example.plantsapp

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.plantsapp.classes.Plant
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.plantsapp.classes.PlantRecyclerView
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_navigation.*
import android.widget.AdapterView




class CreateSeedTimeFragment : Fragment() {

    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseUser: FirebaseUser
    private  lateinit var plantText: AutoCompleteTextView
    private lateinit var sownPlantsText: TextInputEditText
    private lateinit var plantingDateText: TextInputEditText
    private lateinit var seedTimeDescription: TextInputEditText
    private lateinit var cancelButton: Button
    private lateinit var saveButton: Button
    private lateinit var plantsTextLayout: TextInputLayout
    private val args: CreateSeedTimeFragmentArgs by navArgs()
    private var gardenID: String = ""
    private var plant:PlantRecyclerView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_seed_time, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        db = FirebaseFirestore.getInstance()
        firebaseUser = FirebaseAuth.getInstance().currentUser!!
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        plantText = view.findViewById(R.id.plants_dropdown_id)
        plantsTextLayout = view.findViewById(R.id.plantsTextLayout_id)
        sownPlantsText = view.findViewById(R.id.sownPlants_id)
        plantingDateText = view.findViewById(R.id.plantingDate_id)
        seedTimeDescription = view.findViewById(R.id.seedTimeDescription_id)
        cancelButton = view.findViewById(R.id.cancelButton_id)
        saveButton = view.findViewById(R.id.saveButton_id)
        loadItemsInDropdown(loadPlants(), R.id.plants_dropdown_id)
        gardenID = args.gardenID
        plantingDateText.isFocusable = false
        plantingDateText.setOnClickListener{openDatePicker(plantingDateText, this.requireActivity())}
        saveButton.setOnClickListener { saveNewSeedTime() }
        cancelButton.setOnClickListener { goToSeedTimeFragment() }
        plantText.setOnItemClickListener { parent, _, position, _ ->
                plant = parent.getItemAtPosition(position) as PlantRecyclerView
            }
    }

    private fun loadPlants(): MutableList<PlantRecyclerView> {
        val plantsArray: MutableList<PlantRecyclerView> = mutableListOf()
        var plant: Plant
        db.collection("users")
            .document(firebaseUser.uid).collection("plants")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    plant = document.toObject(Plant::class.java)
                    plantsArray.add(PlantRecyclerView(document.id, plant.plantName, ""))
                    Log.d("Plant", plant.toString())
                }
            }
            .addOnFailureListener { exception ->
                Log.d("GetPlant", "Error getting documents: ", exception)
            }
        if (plantsArray.size == 0){
            addEventForNewPlant()
        }
        return plantsArray
    }

    private fun addEventForNewPlant(){
        plantText.setOnClickListener{
            if (plantText.adapter.isEmpty){
                createMessageDialog()
            }
        }
    }

    private fun createMessageDialog(){
        val builder = AlertDialog.Builder(this.requireContext())
        builder.setTitle("Crear una Nueva Planta")
        builder.setMessage("No tienes plantas creadas. ¿Desea crear una nueva planta?")
        builder.setPositiveButton("Si"){dialog, _ ->
            val nav = mainFragment.findNavController()
            nav.navigate(R.id.createPlantFragment)
            builder.setCancelable(true)
            dialog.dismiss()
        }
        builder.setNegativeButton("No"){_, _ ->
        }
        val dialog: AlertDialog = builder.create()

        dialog.show()
    }

    private fun loadItemsInDropdown(items: MutableList<PlantRecyclerView>, dropdown: Int) {
        val adapter = ArrayAdapter(
            this.requireContext(),
            R.layout.dropdown_menu_popup_item,
            items
        )
        val editTextFilledExposedDropdown = view?.findViewById<AutoCompleteTextView>(dropdown)
        editTextFilledExposedDropdown?.setAdapter(adapter)
    }

    private fun saveNewSeedTime(){
        val sownPlants = sownPlantsText.text.toString().toInt()
        val plantingDate = plantingDateText.text.toString().split("-")
        val seedTimeDescription = seedTimeDescription.text.toString()

        val newSeedTime = hashMapOf(
            "plantID" to plant!!.plantID,
            "sownPlants" to sownPlants,
            "plantingDate" to putDateInFormatDate(plantingDate),
            "seedTimeDescription" to seedTimeDescription
        )

        db.collection("users")
            .document(firebaseUser.uid).collection("gardens").document(gardenID).collection("seedtime")
            .add(newSeedTime)
            .addOnSuccessListener { documentReference ->
                Log.d("SaveSeedTime", "DocumentSnapshot added with ID: ${documentReference.id}")
                Toast.makeText(this.requireContext(), "Datos Guardados", Toast.LENGTH_LONG).show()
                goToSeedTimeFragment()
            }
            .addOnFailureListener { e ->
                Log.w("SaveSeedTime", "Error adding document", e)
                Toast.makeText(this.requireContext(), "Error, no se pudo guardar la información", Toast.LENGTH_LONG).show()
            }
    }

    private fun goToSeedTimeFragment(){
        val action = CreateSeedTimeFragmentDirections.actionCreateSeedTimeFragmentToSeedTimeFragment(gardenID)
        view?.findNavController()?.navigate(action)
    }
}
