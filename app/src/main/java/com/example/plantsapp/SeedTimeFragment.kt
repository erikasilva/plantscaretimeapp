package com.example.plantsapp


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.classes.Garden
import com.example.plantsapp.recyclerViewSeedTime.SeedTimeRecyclerViewAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class SeedTimeFragment : Fragment() {


    lateinit var seedTimeRecyclerView: RecyclerView
    private lateinit var storage: FirebaseStorage
    private lateinit var storageRef: StorageReference
    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseUser: FirebaseUser
    private lateinit var addSeedTimeButton: FloatingActionButton
    private lateinit var gardenTitle: TextView
    private lateinit var editGardenButton: Button
    private lateinit var viewNotificationsButton: Button
    private val args: SeedTimeFragmentArgs by navArgs()
    var gardenID: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_seed_time, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        db = FirebaseFirestore.getInstance()
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference
        firebaseUser = FirebaseAuth.getInstance().currentUser!!
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addSeedTimeButton = view.findViewById(R.id.addSeedTimeButton_id)
        addSeedTimeButton.setOnClickListener {goToCreateNewSeedTimeFragment()}
        gardenTitle = view.findViewById(R.id.gardenTitle_id)
        editGardenButton = view.findViewById(R.id.editButton_id)
        editGardenButton.setOnClickListener { goToEditGarden() }
        viewNotificationsButton = view.findViewById(R.id.viewNotificationsButton_id)
        viewNotificationsButton.setOnClickListener { goToViewNotifications() }
        gardenID = args.gardenID
        getGardenInformation()
        seedTimeRecyclerView  = view.findViewById(R.id.seedTimeRecyclerView_id)
        //seedTimeRecyclerView.layoutManager = GridLayoutManager(this.context, 2, RecyclerView.VERTICAL, false)
        seedTimeRecyclerView.layoutManager = LinearLayoutManager(this.requireContext())
        seedTimeRecyclerView.adapter = SeedTimeRecyclerViewAdapter(gardenID)
    }

    private fun getGardenInformation(){
        val plantRef = db.collection("users").document(firebaseUser.uid).collection("gardens").document(gardenID)
        var garden: Garden?
        plantRef.get().addOnSuccessListener { documentSnapshot ->
            garden = documentSnapshot.toObject(Garden::class.java)
            gardenTitle.text = garden!!.gardenName
        }
    }

    private fun goToEditGarden(){
        val action = SeedTimeFragmentDirections.actionSeedTimeFragmentToEditGardenFragment(gardenID)
        view?.findNavController()?.navigate(action)
    }

    private fun goToCreateNewSeedTimeFragment(){
        val action = SeedTimeFragmentDirections.actionSeedTimeFragmentToCreateSeedTimeFragment(gardenID)
        view?.findNavController()?.navigate(action)
    }

    private fun goToViewNotifications(){
        val action = SeedTimeFragmentDirections.actionSeedTimeFragmentToViewNotificationsFragment(gardenID)
        view?.findNavController()?.navigate(action)
    }
}

