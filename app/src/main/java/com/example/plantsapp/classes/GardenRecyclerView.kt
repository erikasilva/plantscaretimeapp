package com.example.plantsapp.classes

class GardenRecyclerView(
    var gardenID: String,
    var gardenName: String,
    var imageURL: String
){

    override fun toString(): String {
        return gardenName
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            GardenRecyclerView("", "", "").apply {

            }
    }
}