package com.example.plantsapp.classes

import com.google.firebase.Timestamp

class AlarmRecyclerView(
    var alarmID: String,
    var alarmName: String,
    var alarmDescription: String,
    var alarmDate: Timestamp,
    var imageURL: String
){

}