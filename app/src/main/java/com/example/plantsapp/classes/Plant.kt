package com.example.plantsapp.classes

class Plant{
    var plantName: String = ""
    var plantType: String = ""
    var plantDescription: String = ""
    var plantCategory: String = ""
    var imageURL: String = ""
}
