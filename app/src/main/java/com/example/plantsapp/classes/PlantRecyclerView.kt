package com.example.plantsapp.classes

class PlantRecyclerView(
    var plantID: String,
    var plantName: String,
    var imageURL: String
){
    override fun toString(): String {
        return plantName
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            PlantRecyclerView("", "", "").apply {

            }
    }
}