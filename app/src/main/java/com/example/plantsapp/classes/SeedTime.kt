package com.example.plantsapp.classes

import java.util.*

class SeedTime{
    var plantID: String = ""
    var sownPlants: Int = 0
    var plantingDate: Date? = null
    var seedTimeDescription: String = ""
}