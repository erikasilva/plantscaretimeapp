package com.example.plantsapp.classes

class Garden{
    var gardenName: String = ""
    var gardenType: String = ""
    var gardenLocation: String = ""
    var imageURL: String = ""
    var seedTime: ArrayList<SeedTime>? = null
}