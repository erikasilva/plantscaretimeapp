package com.example.plantsapp.classes

import java.util.*
import kotlin.collections.ArrayList

class User{
    var uid: String = ""
    var username: String = ""
    var name: String = ""
    var email: String = ""
    var birthday: Date? = null
    var imageURL: String = ""
    var gardens: ArrayList<Garden>? = null
    var plants: ArrayList<Plant>? = null
}