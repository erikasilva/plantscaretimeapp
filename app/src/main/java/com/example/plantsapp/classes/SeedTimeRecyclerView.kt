package com.example.plantsapp.classes

class SeedTimeRecyclerView(
    var seedTimeID: String,
    var plantID: String,
    var sownPlants: Int,
    var plantName: String,
    var imageURL: String
){

    override fun toString(): String {
        if (seedTimeID == "-1"){
            return plantName
        }
        return "Siembra de $plantName"
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            SeedTimeRecyclerView("", "", 0,"","").apply {

            }
    }
}