package com.example.plantsapp

import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}

@TargetApi(26)
fun putDateInFormatDate(dateSplit: List<String>): Date {
    val year = dateSplit.component1().trim().toInt()
    val month = dateSplit.component2().trim().toInt()
    val day = dateSplit.component3().trim().toInt()
    val cal: Calendar = Calendar.getInstance()
    cal.set(Calendar.YEAR, year)
    cal.set(Calendar.MONTH, month)
    cal.set(Calendar.DAY_OF_MONTH, day)
    val date: Date = cal.time
    return date
}

fun uploadImage(context: Context, fragment: Fragment){
    val pictureDialog = AlertDialog.Builder(context)
    pictureDialog.setTitle("Seleccionar Acción")
    val pictureDialogItems = arrayOf("Seleccionar foto desde la galería", "Capturar foto desde la cámara")
    pictureDialog.setItems(pictureDialogItems
    ) { dialog, which ->
        when (which) {
            0 -> choosePhotoFromGallery(fragment)
            1 -> takePhotoFromCamera(fragment)
        }
    }
    pictureDialog.show()
}

private val GALLERY = 1
private val CAMERA = 2

private fun choosePhotoFromGallery(fragment: Fragment) {
    val galleryIntent = Intent(
        Intent.ACTION_PICK,
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    fragment.startActivityForResult(galleryIntent, GALLERY)
}

private fun takePhotoFromCamera(fragment: Fragment) {
    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    fragment.startActivityForResult(intent, CAMERA)
}

fun openDatePicker(dateText: TextInputEditText, activity: Activity) {
    val calendar = Calendar.getInstance()
    val year = calendar.get(Calendar.YEAR)
    val month = calendar.get(Calendar.MONTH)
    val day = calendar.get(Calendar.DAY_OF_MONTH)
    val dialog = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener{ _, year, month, day ->
        val date = "$year - ${month + 1} - $day"
        dateText.setText(date)
    }, year, month, day)
    dialog.show()
}

fun openTimePicker(timeText: TextInputEditText, activity: Activity) {
    val calendar = Calendar.getInstance()
    val hour = calendar.get(Calendar.HOUR_OF_DAY)
    val minute = calendar.get(Calendar.MINUTE)
    val dialog = TimePickerDialog(activity, TimePickerDialog.OnTimeSetListener{_, hour, minute ->
        val time = "$hour : $minute"
        timeText.setText(time)
    }, hour, minute, false)
    dialog.show()
}