package com.example.plantsapp


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth

class ForgotPassword : Fragment() {

    private lateinit var auth: FirebaseAuth
    lateinit var emailTextField: TextInputEditText
    lateinit var sendEmail: Button
    lateinit var cancelButton: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        emailTextField = view.findViewById(R.id.emailText_id)
        sendEmail = view.findViewById(R.id.sendEmailButton_id)
        sendEmail.setOnClickListener { sendEmail() }
        cancelButton = view.findViewById(R.id.cancelButton_id)
        cancelButton.setOnClickListener { goToLoginFragment() }
    }

    private fun goToLoginFragment(){
        val action = ForgotPasswordDirections.actionForgotPasswordFragmentToLoginFragment()
        view?.findNavController()?.navigate(action)
    }

    private fun sendEmail(){
        auth.sendPasswordResetEmail(emailTextField.text.toString())
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("ForgotPassword", "Email sent.")
                    Toast.makeText(context, "Email enviado.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }
}
