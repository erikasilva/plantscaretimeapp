package com.example.plantsapp

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class CreateAccountFragment : Fragment() {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    lateinit var emailTextField: TextInputEditText
    lateinit var passwordTextField: TextInputEditText
    lateinit var confirmPasswordTextField: TextInputEditText
    lateinit var createAccountButton: Button
    lateinit var cancelButton: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        emailTextField = view.findViewById(R.id.emailText_id)
        passwordTextField = view.findViewById(R.id.passwordText_id)
        confirmPasswordTextField = view.findViewById(R.id.confirmPasswordText_id)
        createAccountButton = view.findViewById(R.id.sendEmailButton_id)
        createAccountButton.setOnClickListener { createAccount() }
        cancelButton = view.findViewById(R.id.cancelButton_id)
        cancelButton.setOnClickListener { goToLoginFragment() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()
    }

    private fun createAccount(){
        if (!validateForm()) {
            return
        }
        val email = emailTextField.text.toString()
        val password = passwordTextField.text.toString()

        if (passwordTextField.text.toString() == confirmPasswordTextField.text.toString()){
            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d("CA", "createUserWithEmail:success")
                        val user = auth.currentUser
                        saveInformationInFirestore(user!!.uid)
                        goToLoginFragment()
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("CA", "createUserWithEmail:failure", task.exception)
                        Toast.makeText(context, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }

    private fun saveInformationInFirestore(uid: String){
        val userInformation = hashMapOf(
            "username" to "",
            "name" to "",
            "email" to "",
            "birthday" to Date(),
            "imageURL" to ""
        )

        db.collection("users")
            .document(uid)
            .set(userInformation)
            .addOnSuccessListener { documentReference ->
                Log.d("SaveUserInformation", "DocumentSnapshot added with ID: $documentReference")
            }
            .addOnFailureListener { e ->
                Log.w("SaveUserInformation", "Error adding document", e)
            }
    }

    private fun goToLoginFragment(){
        val action = CreateAccountFragmentDirections.actionCreateAccountFragmentToLoginFragment()
        view?.findNavController()?.navigate(action)
    }

    private fun validateForm(): Boolean {
        var valid = true
        val email = emailTextField.text.toString()
        val password = passwordTextField.text.toString()
        val confirmPassword = confirmPasswordTextField.text.toString()

        if (TextUtils.isEmpty(email)) {
            emailTextField.error = "Required."
            valid = false
        } else {
            emailTextField.error = null
        }

        if (TextUtils.isEmpty(password)) {
            passwordTextField.error = "Required."
            valid = false
        } else {
            passwordTextField.error = null
        }

        if (TextUtils.isEmpty(confirmPassword)) {
            confirmPasswordTextField.error = "Required."
            valid = false
        } else {
            confirmPasswordTextField.error = null
        }

        return valid
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            CreateAccountFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
