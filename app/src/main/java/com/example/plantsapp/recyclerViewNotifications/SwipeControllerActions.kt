package com.example.plantsapp.recyclerViewNotifications

abstract class SwipeControllerActions(){
    abstract fun onRightClicked(position: Int)
}