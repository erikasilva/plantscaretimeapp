package com.example.plantsapp


import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.plantsapp.classes.Plant
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso

class EditPlantFragment : Fragment() {

    private lateinit var storage: FirebaseStorage
    private lateinit var storageRef: StorageReference
    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseUser: FirebaseUser
    private lateinit var plantImageButton : ImageButton
    private lateinit var plantTitle: TextView
    private lateinit var plantNameText: TextInputEditText
    private lateinit var plantsTypeText: AutoCompleteTextView
    private lateinit var plantsCategoryText: AutoCompleteTextView
    private lateinit var plantDescriptionText : TextInputEditText
    private lateinit var cancelButton: Button
    private lateinit var saveButton: Button
    private var imageURI: Uri? = null
    private val GALLERY = 1
    private val CAMERA = 2
    private var plantID:String = ""
    private val args: EditPlantFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_plant, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        db = FirebaseFirestore.getInstance()
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference
        firebaseUser = FirebaseAuth.getInstance().currentUser!!
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        plantImageButton = view.findViewById(R.id.plantImage_id)
        plantTitle = view.findViewById(R.id.editPlantTitle)
        plantNameText = view.findViewById(R.id.plantName_id)
        plantDescriptionText = view.findViewById(R.id.plantDescription_id)
        plantsTypeText = view.findViewById(R.id.plantsType_dropdown_id)
        plantsCategoryText = view.findViewById(R.id.categories_dropdown_id)
        cancelButton = view.findViewById(R.id.cancelButton_id)
        saveButton = view.findViewById(R.id.saveButton_id)
        val types = arrayOf("Vegetales", "Flores", "Árboles", "Plantas")
        val categories = arrayOf("Interior", "Aire Libre", "Campo")
        plantID = args.plantID
        Log.d("plantID", plantID)
        getPlantInformation()
        cancelButton.setOnClickListener { goToPlantsFragment() }
        saveButton.setOnClickListener { savePlantInformation() }
        plantImageButton.setOnClickListener { uploadImage(this.requireContext(), this) }

        plantsTypeText.setOnClickListener{
            loadItemsInDropdown(types, R.id.plantsType_dropdown_id)
        }
        plantsCategoryText.setOnClickListener{
            loadItemsInDropdown(categories, R.id.categories_dropdown_id)
        }
    }

    private fun loadItemsInDropdown(items: Array<String>, dropdown: Int) {
        val adapter = ArrayAdapter(
            this.requireContext(),
            R.layout.dropdown_menu_popup_item,
            items
        )
        val editTextFilledExposedDropdown = view?.findViewById<AutoCompleteTextView>(dropdown)
        editTextFilledExposedDropdown?.setAdapter(adapter)
    }


    override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY)
        {
            if (data != null)
            {
                imageURI = data.data
                Picasso.with(this.requireContext()).load(imageURI).into(plantImageButton)
            }
        }
        else if (requestCode == CAMERA)
        {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            plantImageButton.setImageBitmap(thumbnail)
        }
    }

    private fun getPlantInformation(){
        val plantRef = db.collection("users").document(firebaseUser.uid).collection("plants").document(plantID)
        var plant: Plant?
        plantRef.get().addOnSuccessListener { documentSnapshot ->
            plant = documentSnapshot.toObject(Plant::class.java)
            plantTitle.text = plant!!.plantName
            plantNameText.setText(plant!!.plantName)
            plantDescriptionText.setText(plant!!.plantDescription)
            plantsTypeText.setText(plant!!.plantType)
            plantsCategoryText.setText(plant!!.plantCategory)
            if (!plant!!.imageURL.isEmpty()){
                Picasso.with(this.requireContext()).load(plant!!.imageURL).into(plantImageButton)
            }
        }
    }

    private fun savePlantInformation(){
        val plantName = plantNameText.text.toString()
        val plantType = plantsTypeText.text.toString()
        val plantCategory = plantsCategoryText.text.toString()
        val plantDescription = plantDescriptionText.text.toString()

        val editPlant = mapOf(
            "plantName" to plantName,
            "plantType" to plantType,
            "plantCategory" to plantCategory,
            "plantDescription" to plantDescription
        )

        db.collection("users")
            .document(firebaseUser.uid).collection("plants").document(plantID)
            .update(editPlant)
            .addOnSuccessListener { documentReference ->
                Log.d("SavePlant", "DocumentSnapshot added with ID: $documentReference")
                Toast.makeText(this.requireContext(), "Datos Guardados", Toast.LENGTH_LONG).show()
                if (imageURI != null){
                    saveImageInFirebase(plantID)
                }
            }
            .addOnFailureListener { e ->
                Log.w("SavePlant", "Error adding document", e)
                Toast.makeText(this.requireContext(), "Error, no se pudo guardar la información", Toast.LENGTH_LONG).show()
            }
        goToPlantsFragment()
    }

    private fun saveImageURLInFirestore(imageURL: String, id: String){
        Log.d("plantID", id)
        db.collection("users")
            .document(firebaseUser.uid).collection("plants").document(id)
            .update("imageURL", imageURL)
            .addOnSuccessListener { documentReference ->
                Log.d("SaveImageURL", "DocumentSnapshot added with ID: $documentReference")
            }
            .addOnFailureListener { e ->
                Log.w("SaveImageURL", "Error adding document", e)
            }
    }

    private fun saveImageInFirebase(id: String){
        val imagesRef: StorageReference? = storageRef.child("images/${firebaseUser.uid}/plants/$id")
        val uploadTask = imagesRef?.putFile(imageURI!!)
        uploadTask?.addOnFailureListener {
            Log.d("SaveImage", "Error")
        }?.addOnSuccessListener {
            imagesRef.downloadUrl.addOnSuccessListener {
                    uri -> saveImageURLInFirestore(uri.toString(), id)
            }
            Log.d("SaveImage", "File upload")
        }
    }

    private fun goToPlantsFragment(){
        val action = EditPlantFragmentDirections.actionEditPlantFragmentToPlantsFragment()
        view?.findNavController()?.navigate(action)
    }

}
