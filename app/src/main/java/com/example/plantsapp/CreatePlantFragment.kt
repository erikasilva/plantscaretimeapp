package com.example.plantsapp


import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.findNavController
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso


class CreatePlantFragment : Fragment() {

    private lateinit var storage: FirebaseStorage
    private lateinit var storageRef: StorageReference
    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseUser: FirebaseUser
    lateinit var plantImageButton : ImageButton
    lateinit var plantNameText: TextInputEditText
    lateinit var plantsTypeText: AutoCompleteTextView
    lateinit var plantsCategoryText: AutoCompleteTextView
    lateinit var plantDescriptionText : TextInputEditText
    lateinit var cancelButton: Button
    lateinit var saveButton: Button
    private var imageURI: Uri? = null
    private val GALLERY = 1
    private val CAMERA = 2

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_plant, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        db = FirebaseFirestore.getInstance()
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference
        firebaseUser = FirebaseAuth.getInstance().currentUser!!
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /*plantsType = view.findViewById(R.id.plantsTypeSpinner_id)
        loadItemsOnSpinner(R.array.plantsType, plantsType)
        plantsCategory = view.findViewById(R.id.plantsCategorySpinner_id)
        loadItemsOnSpinner(R.array.plantsCategory, plantsCategory)*/
        plantImageButton = view.findViewById(R.id.plantImage_id)
        plantNameText = view.findViewById(R.id.plantName_id)
        plantDescriptionText = view.findViewById(R.id.plantDescription_id)
        plantsTypeText = view.findViewById(R.id.plantsType_dropdown_id)
        plantsCategoryText = view.findViewById(R.id.categories_dropdown_id)
        cancelButton = view.findViewById(R.id.cancelButton_id)
        cancelButton.setOnClickListener { goToPlantsFragment() }
        saveButton = view.findViewById(R.id.saveButton_id)
        saveButton.setOnClickListener { savePlantInformation() }
        plantImageButton.setOnClickListener { uploadImage(this.requireContext(), this) }

        val types = arrayOf("Vegetales", "Flores", "Árboles", "Plantas")
        val categories = arrayOf("Interior", "Aire Libre", "Campo")
        loadItemsInDropdown(types, R.id.plantsType_dropdown_id)
        loadItemsInDropdown(categories, R.id.categories_dropdown_id)
    }

    private fun loadItemsInDropdown(items: Array<String>, dropdown: Int) {
        val adapter = ArrayAdapter(
            this.requireContext(),
            R.layout.dropdown_menu_popup_item,
            items
        )
        val editTextFilledExposedDropdown = view?.findViewById<AutoCompleteTextView>(dropdown)
        editTextFilledExposedDropdown?.setAdapter(adapter)
    }

    private fun loadItemsInSpinner(resourceName: Int, spinner: Spinner) {
        val adapter = ArrayAdapter.createFromResource(
            this.requireContext(),
            resourceName, android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

    override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY)
        {
            if (data != null)
            {
                imageURI = data.data
                Picasso.with(this.requireContext()).load(imageURI).into(plantImageButton)
            }
        }
        else if (requestCode == CAMERA)
        {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            plantImageButton.setImageBitmap(thumbnail)
        }
    }

    private fun savePlantInformation(){
        val plantName = plantNameText.text.toString()
        val plantType = plantsTypeText.text.toString()
        val plantCategory = plantsCategoryText.text.toString()
        val plantDescription = plantDescriptionText.text.toString()

        val newPlant = hashMapOf(
            "plantName" to plantName,
            "plantType" to plantType,
            "plantCategory" to plantCategory,
            "plantDescription" to plantDescription,
            "imageURL" to ""
        )

        db.collection("users")
            .document(firebaseUser.uid).collection("plants")
            .add(newPlant)
            .addOnSuccessListener { documentReference ->
                Log.d("SavePlant", "DocumentSnapshot added with ID: ${documentReference.id}}")
                Toast.makeText(this.requireContext(), "Datos Guardados", Toast.LENGTH_LONG).show()
                if (imageURI != null){
                    saveImageInFirebase(documentReference.id)
                }
            }
            .addOnFailureListener { e ->
                Log.w("SavePlant", "Error adding document", e)
                Toast.makeText(this.requireContext(), "Error, no se pudo guardar la información", Toast.LENGTH_LONG).show()
            }
    }

    private fun saveImageURLInFirestore(imageURL: String, id: String){
        Log.d("plantID", id)
        db.collection("users")
            .document(firebaseUser.uid).collection("plants").document(id)
            .update("imageURL", imageURL)
            .addOnSuccessListener { documentReference ->
                Log.d("SaveImageURL", "DocumentSnapshot added with ID: $documentReference")
            }
            .addOnFailureListener { e ->
                Log.w("SaveImageURL", "Error adding document", e)
            }
        goToPlantsFragment()
    }

    private fun saveImageInFirebase(id: String){
        val imagesRef: StorageReference? = storageRef.child("images/${firebaseUser.uid}/plants/$id")
        val uploadTask = imagesRef?.putFile(imageURI!!)
        uploadTask?.addOnFailureListener {
            Log.d("SaveImage", "Error")
        }?.addOnSuccessListener {
            imagesRef.downloadUrl.addOnSuccessListener {
                    uri -> saveImageURLInFirestore(uri.toString(), id)
            }
            Log.d("SaveImage", "File upload")
        }
    }

    private fun goToPlantsFragment(){
        val action = CreatePlantFragmentDirections.actionCreatePlantFragmentToPlantsFragment()
        view?.findNavController()?.navigate(action)
    }
}

