package com.example.plantsapp


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.classes.Garden
import com.example.plantsapp.recyclerViewNotifications.ViewNotificationsRecyclerViewAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore

class ViewNotificationsFragment : Fragment() {

    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseUser: FirebaseUser
    private lateinit var notificationsRecyclerView: RecyclerView
    private lateinit var gardenTitle: TextView
    private val args: ViewNotificationsFragmentArgs by navArgs()
    var gardenID: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_notifications, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        db = FirebaseFirestore.getInstance()
        firebaseUser = FirebaseAuth.getInstance().currentUser!!
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        gardenID = args.gardenID
        gardenTitle = view.findViewById(R.id.gardenTitle_id)
        getGardenInformation()
        val notificationsRecyclerViewAdapter = ViewNotificationsRecyclerViewAdapter(gardenID)
        notificationsRecyclerView  = view.findViewById(R.id.viewNotificationsRecyclerView_id)
        notificationsRecyclerView.layoutManager = LinearLayoutManager(this.requireContext())
        notificationsRecyclerView.adapter = notificationsRecyclerViewAdapter
    }

    private fun getGardenInformation(){
        val plantRef = db.collection("users").document(firebaseUser.uid).collection("gardens").document(gardenID)
        var garden: Garden?
        plantRef.get().addOnSuccessListener { documentSnapshot ->
            garden = documentSnapshot.toObject(Garden::class.java)
            gardenTitle.text = garden!!.gardenName
        }
    }
}
