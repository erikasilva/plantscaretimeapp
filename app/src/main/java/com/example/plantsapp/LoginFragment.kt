package com.example.plantsapp


import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : Fragment() {

    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var googleSignInClient: GoogleSignInClient
    lateinit var loginButton: Button
    lateinit var createAccountButton: Button
    lateinit var forgotPasswordButton: Button
    lateinit var googleButton: Button
    lateinit var emailTextField: TextInputEditText
    lateinit var passwordTextField: TextInputEditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        emailTextField = view.findViewById(R.id.emailText_id)
        passwordTextField = view.findViewById(R.id.passwordText_id)

        loginButton = view.findViewById(R.id.saveButton_id)
        loginButton.setOnClickListener { login(emailTextField.text.toString(), passwordTextField.text.toString()) }

        createAccountButton = view.findViewById(R.id.sendEmailButton_id)
        createAccountButton.setOnClickListener { goToCreateAccountFragment() }

        googleButton = view.findViewById(R.id.googleButton_id)
        googleButton.setOnClickListener{signInWithGoogle()}

        forgotPasswordButton = view.findViewById(R.id.forgotPasswordButton_id)
        forgotPasswordButton.setOnClickListener { goToForgotPasswordFragment() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        db = FirebaseFirestore.getInstance()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this.requireContext(), gso)
        auth = FirebaseAuth.getInstance()
    }

    private fun signInWithGoogle() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            } catch (e: ApiException) {
                Log.w(TAG, "Google sign in failed", e)
            }
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.id!!)
        showProgressDialog()

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                    saveInformationInFirestore(user!!.uid)
                    goToNavigationActivity(user.uid)
                    hideProgressDialog()
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    Snackbar.make(this.requireView(), "Authentication Failed.", Snackbar.LENGTH_SHORT).show()
                }
                hideProgressDialog()
            }
    }

    private fun saveInformationInFirestore(uid: String){
        val userInformation = hashMapOf(
            "username" to "",
            "name" to "",
            "email" to "",
            "birthday" to Date(),
            "imageURL" to ""
        )

        db.collection("users")
            .document(uid)
            .set(userInformation)
            .addOnSuccessListener { documentReference ->
                Log.d("SaveUserInformation", "DocumentSnapshot added with ID: $documentReference")
            }
            .addOnFailureListener { e ->
                Log.w("SaveUserInformation", "Error adding document", e)
            }
    }

    private fun login(email: String, password: String){
        if (!validateForm()) {
            return
        }
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("", "signInWithEmail:success")
                    val user = auth.currentUser
                    Log.d("user", user?.email)
                    goToNavigationActivity(user?.uid.toString())
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("", "signInWithEmail:failure", task.exception)
                    Toast.makeText(context, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    cleanFields()
                }
            }
    }

    private fun cleanFields(){
        passwordTextField.setText("")
        emailTextField.setText("")
    }

    private fun goToNavigationActivity(userID: String){
        val action = LoginFragmentDirections.actionLoginFragmentToNavigationActivity(userID)
        view?.findNavController()?.navigate(action)
    }

    private fun goToCreateAccountFragment(){
        val action = LoginFragmentDirections.actionLoginFragmentToCreateAccountFragment()
        view?.findNavController()?.navigate(action)
    }

    private fun goToForgotPasswordFragment(){
        val action = LoginFragmentDirections.actionLoginFragmentToForgotPassword()
        view?.findNavController()?.navigate(action)
    }

    private fun validateForm(): Boolean {
        var valid = true

        val email = emailTextField.text.toString()
        if (TextUtils.isEmpty(email)) {
            emailTextField.error = "Required."
            valid = false
        } else {
            emailTextField.error = null
        }

        val password = passwordTextField.text.toString()
        if (TextUtils.isEmpty(password)) {
            passwordTextField.error = "Required."
            valid = false
        } else {
            passwordTextField.error = null
        }

        return valid
    }

    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        Log.d("User", currentUser?.email.toString())
        if(currentUser != null){
            goToNavigationActivity(currentUser?.uid.toString())
        }
    }

    companion object {
        private const val TAG = "GoogleActivity"
        private const val RC_SIGN_IN = 9001
    }


    val progressDialog by lazy {
        ProgressDialog(this.requireContext())
    }

    fun showProgressDialog() {
        progressDialog.setMessage(getString(R.string.loading))
        progressDialog.isIndeterminate = true
        progressDialog.show()
    }

    fun hideProgressDialog() {
        if (progressDialog.isShowing) {
            progressDialog.dismiss()
        }
    }

    public override fun onStop() {
        super.onStop()
        hideProgressDialog()
    }
}
