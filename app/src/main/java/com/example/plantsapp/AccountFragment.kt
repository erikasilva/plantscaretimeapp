package com.example.plantsapp

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import com.example.plantsapp.classes.User
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.model.Document
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_account.*
import java.util.*

class AccountFragment : Fragment() {

    private lateinit var storage: FirebaseStorage
    private lateinit var storageRef: StorageReference
    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseUser: FirebaseUser
    private lateinit var signOutButton: Button
    private lateinit var saveButton: Button
    private lateinit var cancelButton: Button
    private lateinit var editButton: Button
    private lateinit var userNameText: TextView
    private lateinit var plantsNumberText: TextView
    private lateinit var gardensNumberText: TextView
    private lateinit var nameText: TextInputEditText
    private lateinit var emailText: TextInputEditText
    private lateinit var birthdayText: TextInputEditText
    private lateinit var imageAccountButton: ImageButton
    private var user: User? = null
    private var document: Document? = null
    private var imageURI: Uri? = null
    private val GALLERY = 1
    private val CAMERA = 2

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        db = FirebaseFirestore.getInstance()
        storage = FirebaseStorage.getInstance()
        storageRef = storage.reference
        firebaseUser = FirebaseAuth.getInstance().currentUser!!
        if (document == null){
            db.collection("users").document(firebaseUser.uid).get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        Log.d("GetUserInfo", "DocumentSnapshot data: ${document.data}")
                        user = document.toObject(User::class.java)
                        Log.d("GetUserInfo", "DocumentSnapshot data: ${user?.birthday}")
                        loadInformation(user!!)
                        loadImage(user!!.imageURL)
                    } else {
                        Log.d("GetUserInfo", "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d("GetUserInfo", "get failed with ", exception)
                }
        }
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editButton = view.findViewById(R.id.editButton_id)
        saveButton = view.findViewById(R.id.saveButton_id)
        cancelButton = view.findViewById(R.id.cancelButton_id)
        signOutButton = view.findViewById(R.id.signOutButton_id)
        imageAccountButton = view.findViewById(R.id.imageAccountButton_id)
        userNameText = view.findViewById(R.id.username_id)
        plantsNumberText = view.findViewById(R.id.plantsNumber_id)
        gardensNumberText = view.findViewById(R.id.gardensNumber_id)
        nameText = view.findViewById(R.id.nameText_id)
        emailText = view.findViewById(R.id.emailText_id)
        birthdayText = view.findViewById(R.id.birthdayText_id)
        birthdayText.setOnClickListener{ openDatePicker(birthdayText, this.requireActivity()) }

        imageAccountButton.setOnClickListener{uploadImage(this.requireContext(), this)}
        editButton.setOnClickListener { editFields() }
        cancelButton.setOnClickListener { cancelEditMode() }
        saveButton.setOnClickListener { saveProfileInformation() }
        signOutButton.setOnClickListener { signOut() }

        imageAccountButton.isEnabled = false
        cancelButton.isVisible = false
        saveButton.isVisible = false
        nameText.isEnabled = false
        emailText.isEnabled = false
        birthdayText.isEnabled = false
        userNameText.isEnabled = false
    }

    private fun loadInformation(user: User){
        userNameText.text = user.username
        nameText.setText(user.name)
        emailText.setText(user.email)
        val dateInFormat = putDateInFormatText(user.birthday!!)
        birthdayText.setText(dateInFormat)
        getPlantsNumber()
        getGardensNumber()
    }

    private fun getPlantsNumber(){
         db.collection("users").document(firebaseUser.uid).collection("plants")
            .get()
            .addOnCompleteListener{
                task -> setPlantsNumber(task.result!!.size())
            }
    }

    private fun setPlantsNumber(plantsNumber: Int){
        plantsNumberText.setText("Plantas: $plantsNumber")
    }

    private fun getGardensNumber(){
        db.collection("users").document(firebaseUser.uid).collection("gardens")
            .get()
            .addOnCompleteListener{
                    task -> setGardensNumber(task.result!!.size())
            }
    }

    private fun setGardensNumber(gardensNumber: Int){
        gardensNumberText.setText("Jardines: $gardensNumber")
    }

    private fun putDateInFormatText(date: Date):String{
        return "${date.year+1900} - ${date.month+1} - ${date.date}"
    }

    private fun loadImage(imageURL: String) {
        if (!imageURL.isEmpty()){
            Picasso.with(this.requireContext()).load(imageURL).into(imageAccountButton)
        }
    }

    private fun editFields(){
        saveButton.isVisible = true
        imageAccountButton.isEnabled = true
        cancelButton.isVisible = true
        editButton.isVisible = false
        nameText.isEnabled = true
        birthdayText.isEnabled = true
        birthdayText.isFocusable = false
        userNameText.isEnabled = true
    }

    private fun cancelEditMode(){
        disableFieldsOfEditMode()
    }

    private fun disableFieldsOfEditMode(){
        editButton.isVisible = true
        imageAccountButton.isEnabled = false
        cancelButton.isVisible = false
        saveButton.isVisible = false
        nameText.isEnabled = false
        birthdayText.isEnabled = false
        userNameText.isEnabled = false
    }

    private fun saveProfileInformation(){
        disableFieldsOfEditMode()
        if (imageURI != null){
            saveImageInFirebase()
        }else{
            saveUserInFirestore("")
        }
    }

    private fun saveUserInFirestore(imageURL: String){
        val username = userNameText.text.toString()
        val name = nameText.text.toString()
        val email = emailText.text.toString()
        val birthdaySplit = birthdayText.text.toString().split("-")

        val userInformation = hashMapOf(
            "username" to username,
            "name" to name,
            "email" to email,
            "birthday" to putDateInFormatDate(birthdaySplit),
            "imageURL" to imageURL
        )

        db.collection("users")
            .document(firebaseUser.uid)
            .set(userInformation)
            .addOnSuccessListener { documentReference ->
                Log.d("SaveUserInformation", "DocumentSnapshot added with ID: $documentReference")
                Toast.makeText(this.requireContext(), "Información Actualizada", Toast.LENGTH_LONG).show()
            }
            .addOnFailureListener { e ->
                Log.w("SaveUserInformation", "Error adding document", e)
                Toast.makeText(this.requireContext(), "Error, no se pudo actualizar la información", Toast.LENGTH_LONG).show()
            }
    }

    private fun saveImageInFirebase(){
        val imagesRef: StorageReference? = storageRef.child("images/${firebaseUser.uid}/${firebaseUser.uid}")
        val uploadTask = imagesRef?.putFile(imageURI!!)
        uploadTask?.addOnFailureListener {
            Log.d("SaveImage", "Error")
        }?.addOnSuccessListener {
            imagesRef.downloadUrl.addOnSuccessListener {
                uri -> saveUserInFirestore(uri.toString())
            }
            Log.d("SaveImage", "Imagen cargada")
        }
    }

    override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY)
        {
            if (data != null)
            {
                imageURI = data.data
                Picasso.with(this.requireContext()).load(imageURI).into(imageAccountButton)
            }
        }
        else if (requestCode == CAMERA)
        {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            imageAccountButton.setImageBitmap(thumbnail)
        }
    }

    private fun signOut(){
        activity?.finish()
        startActivity(Intent(context, MainActivity::class.java))
        FirebaseAuth.getInstance().signOut()
    }
}
