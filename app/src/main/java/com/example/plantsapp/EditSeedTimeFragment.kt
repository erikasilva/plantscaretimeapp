package com.example.plantsapp


import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.plantsapp.classes.Plant
import com.example.plantsapp.classes.PlantRecyclerView
import com.example.plantsapp.classes.SeedTime
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_navigation.*
import java.util.*
import kotlin.math.log

class EditSeedTimeFragment : Fragment() {

    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseUser: FirebaseUser
    lateinit var plantText: AutoCompleteTextView
    lateinit var sownPlantsText: TextInputEditText
    lateinit var plantingDateText: TextInputEditText
    lateinit var seedTimeDescription: TextInputEditText
    lateinit var cancelButton: Button
    lateinit var saveButton: Button
    lateinit var seedTimeTitle: TextView
    private val args: EditSeedTimeFragmentArgs by navArgs()
    var gardenID: String = ""
    var seedTimeID: String = ""
    var plant: PlantRecyclerView? = PlantRecyclerView("", "", "")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_seed_time, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        db = FirebaseFirestore.getInstance()
        firebaseUser = FirebaseAuth.getInstance().currentUser!!
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        plantText = view.findViewById(R.id.plants_dropdown_id)
        seedTimeTitle = view.findViewById(R.id.editSeedTimeTitle)
        sownPlantsText = view.findViewById(R.id.sownPlants_id)
        plantingDateText = view.findViewById(R.id.plantingDate_id)
        seedTimeDescription = view.findViewById(R.id.seedTimeDescription_id)
        cancelButton = view.findViewById(R.id.cancelButton_id)
        saveButton = view.findViewById(R.id.saveButton_id)
        gardenID = args.gardenID
        seedTimeID = args.seedTimeID
        plantingDateText.isFocusable = false
        plantingDateText.setOnClickListener{openDatePicker(plantingDateText, this.requireActivity())}
        saveButton.setOnClickListener { saveNewSeedTime() }
        cancelButton.setOnClickListener { goToSeedTimeFragment() }
        plantText.setOnItemClickListener { parent, _, position, _ ->
            plant = parent.getItemAtPosition(position) as PlantRecyclerView
        }
        plantText.setOnClickListener{
            loadItemsInDropdown(loadPlants(), R.id.plants_dropdown_id)
        }
        getSeedTimeInformation()
    }

    private fun getSeedTimeInformation(){
        val seedTimeRef = db.collection("users").document(firebaseUser.uid)
            .collection("gardens").document(gardenID).collection("seedtime").document(seedTimeID)
        var seedTime: SeedTime?
        seedTimeRef.get().addOnSuccessListener { documentSnapshot ->
            seedTime = documentSnapshot.toObject(SeedTime::class.java)
            plant = PlantRecyclerView(seedTime!!.plantID, "", "")
            getPlantInformation(seedTime!!.plantID)
            plantingDateText.setText(putDateInFormatText(seedTime!!.plantingDate!!))
            sownPlantsText.setText(seedTime!!.sownPlants.toString())
            seedTimeDescription.setText(seedTime!!.seedTimeDescription)
        }
    }

    private fun getPlantInformation(plantID: String){
        val plantRef = db.collection("users").document(firebaseUser.uid).collection("plants").document(plantID)
        var plant: Plant?
        plantRef.get().addOnSuccessListener { documentSnapshot ->
            plant = documentSnapshot.toObject(Plant::class.java)
            seedTimeTitle.setText("Siembra de ${plant!!.plantName}")
            plantText.setText(plant!!.plantName)
        }
    }

    private fun putDateInFormatText(date: Date):String{
        return "${date.year+1900} - ${date.month+1} - ${date.date}"
    }

    private fun loadPlants(): MutableList<PlantRecyclerView> {
        val plantsArray: MutableList<PlantRecyclerView> = mutableListOf()
        var plant: Plant
        db.collection("users")
            .document(firebaseUser.uid).collection("plants")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    plant = document.toObject(Plant::class.java)
                    plantsArray.add(PlantRecyclerView(document.id, plant.plantName, ""))
                }
            }
            .addOnFailureListener { exception ->
                Log.d("GetPlant", "Error getting documents: ", exception)
            }
        return plantsArray
    }

    private fun loadItemsInDropdown(items: MutableList<PlantRecyclerView>, dropdown: Int) {
        val adapter = ArrayAdapter(
            this.requireContext(),
            R.layout.dropdown_menu_popup_item,
            items
        )
        val editTextFilledExposedDropdown = view?.findViewById<AutoCompleteTextView>(dropdown)
        editTextFilledExposedDropdown?.setAdapter(adapter)
    }

    private fun saveNewSeedTime(){
        val sownPlants = sownPlantsText.text.toString().toInt()
        val plantingDate = plantingDateText.text.toString().split("-")
        val seedTimeDescription = seedTimeDescription.text.toString()

        val editSeedTime = hashMapOf(
            "plantID" to plant!!.plantID,
            "sownPlants" to sownPlants,
            "plantingDate" to putDateInFormatDate(plantingDate),
            "seedTimeDescription" to seedTimeDescription
        )

        db.collection("users")
            .document(firebaseUser.uid).collection("gardens").document(gardenID).collection("seedtime").document(seedTimeID)
            .update(editSeedTime)
            .addOnSuccessListener { documentReference ->
                Log.d("SaveSeedTime", "DocumentSnapshot added with ID: $documentReference")
                Toast.makeText(this.requireContext(), "Datos Guardados", Toast.LENGTH_LONG).show()
                goToSeedTimeFragment()
            }
            .addOnFailureListener { e ->
                Log.w("SaveSeedTime", "Error adding document", e)
                Toast.makeText(this.requireContext(), "Error, no se pudo guardar la información", Toast.LENGTH_LONG).show()
            }
    }

    private fun goToSeedTimeFragment(){
        val action = EditSeedTimeFragmentDirections.actionEditSeedTimeFragmentToSeedTimeFragment(gardenID)
        view?.findNavController()?.navigate(action)
    }
}
