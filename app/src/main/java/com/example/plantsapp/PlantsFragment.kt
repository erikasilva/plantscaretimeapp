package com.example.plantsapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.recyclerViewPlants.PlantsRecyclerViewAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

class PlantsFragment : Fragment() {

    lateinit var addPlantButton: FloatingActionButton
    lateinit var plantsRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_plants, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addPlantButton = view.findViewById(R.id.addPlantButton_id)
        addPlantButton.setOnClickListener {goToCreateNewPlantFragment()}
        plantsRecyclerView  = view.findViewById(R.id.plantsRecyclerView_id)
        plantsRecyclerView.layoutManager = GridLayoutManager(this.context, 2, RecyclerView.VERTICAL, false)
        plantsRecyclerView.adapter = PlantsRecyclerViewAdapter()
    }

    private fun goToCreateNewPlantFragment(){
        val action = PlantsFragmentDirections.actionPlantsFragmentToCreatePlantFragment()
        view?.findNavController()?.navigate(action)
    }
}
