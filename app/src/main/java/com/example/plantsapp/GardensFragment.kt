package com.example.plantsapp


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.GridLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.plantsapp.recyclerViewGardens.GardensRecyclerViewAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

class GardensFragment : Fragment() {

    lateinit var addGardenButton: FloatingActionButton
    lateinit var gardensRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gardens, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addGardenButton = view.findViewById(R.id.addGardenButton_id)
        addGardenButton.setOnClickListener {goToCreateNewGardenFragment()}

        gardensRecyclerView  = view.findViewById(R.id.gardensRecyclerView_id)
        gardensRecyclerView.layoutManager = GridLayoutManager(this.context, 2, RecyclerView.VERTICAL, false)
        gardensRecyclerView.adapter = GardensRecyclerViewAdapter()

    }

    private fun goToCreateNewGardenFragment(){
        val action = GardensFragmentDirections.actionGardensFragmentToCreateGardenFragment()
        view?.findNavController()?.navigate(action)
    }
}
